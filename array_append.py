#Append(Add an element at the end of array)
#Syntax: list.append(element)  element of any type

#Array of city names
city_names = ["Delhi", "Meerut"]

for city in city_names:
   print(city)

#Append Bangalore to city names
city_names.append("Bangalore")

for city in city_names:
   print(city)

#Append an array to another array
arr1 = ["One", "Two"]
arr2 = ["Three", "Four"]

arr1.append(arr2)

print(len(arr1))
#Loop through array1
for count in arr1:
  print(count)

