#Count: return number of elements with specified value
#Syntax: list.count(value) value: any type

numbers = [1, 2, 3, 4, 1, 2, 5, 6, 1, 1]

count_1 = numbers.count(1)

print(count_1)
