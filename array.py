#array in python
test_arr = ["One", "Two", "Three"]

print(test_arr[0])
print(test_arr[1])
print(test_arr[2])

#Modify an element in array
test_arr[0] = "Change Value"

print(test_arr[0])
print(test_arr[1])
print(test_arr[2])

#Get number of elements in an array
test_arr_len = len(test_arr)
print(test_arr_len)

#Loop through array
for count in test_arr:
  print(count)

#append an element in array
numbers = ["One", "Two", "Three"]
numbers.append("Four")
print(len(numbers))

for count in numbers:
   print(count)

#Delete an element in array from index position
numbers.pop(1)
print(len(numbers))

for count in numbers:
   print(count)

#Delete an element in array from name
numbers.remove("One")
print(len(numbers))

for count in numbers:
   print(count)


