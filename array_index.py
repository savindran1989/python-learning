#Index: return the index of first element with specified value
#syntax: list.index(element)

numbers = [1, 2, 3, 4, 5, 4, 1]

numbers_1_index = numbers.index(4)

print(numbers_1_index)
