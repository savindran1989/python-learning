#This explains to create functions in python

#TestFun having default parameter 5 
#If nothing is passed to this function it will take 5 
def TestFun(arg1 = 5):
  print(arg1)

#function accepting list as an argument
def Fun1(names):
  for count in names:
    print(count)

#return statement in function
def Square(value):
   value = value * value
   return value

def Fun2(var1, var2, var3):
   print(var1)
   print(var2)
   print(var3)

#Function with arbitray arguments
def Fun3(*var):
   print(var[0])
   print(var[1])
   print(var[2])

#Function with empty definition
def Fun4():
   pass

#Test Code
TestFun()
TestFun("Hello")

names = ["Savindra", "Meerut"]
Fun1(names)

variable = Square(10)
print(variable)

#order or arguments does not matter if used key = value
Fun2(var3 = 10, var1 = 11, var2 = 12)

Fun3(5, 6, 7)
