#Lambda function: Small anonymous function
#Accepts any number of arguments and accept only one expression
#It returns function object that can be assigned to any identifier
#Syntax: lambda arguments : expression

value = lambda var : var + 10
print(value(5))

Multiplication = lambda var1, var2 : var1 * var2
print(Multiplication(5, 10))

Sum = lambda var1, var2, var3 : var1 + var2 + var3
print(Sum(11, 12, 13))


#Use of lambda function
def Fun(value):
    result = lambda var : var * value
    return result

doubler = Fun(2)
tripler = Fun(3)
result  = doubler(11)
result1 = tripler(11)
print(result)
print(result1)
