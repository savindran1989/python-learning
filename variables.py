#Variables are created once values are assigned to them
#No need to declare type of variable in python
var1 = 5

print("This is variables example in python")
print(var1)

#Variable can have different type value also
var1 = "Savindra Kumar"

print(var1)

var1 = 1.5

print(var1)

#String can be declared using single or double quote
str_var  = "Hello Savindra"
str_var1 = 'Hey Savi'

print(str_var)
print(str_var1)

#Assign values to variables in one line
var1 , var2, var3 = 5, 6, 7

print(var1)
print(var2)
print(var3)

#Assign same values to multiple variables in one line
var1 = var2 =var3 = 10

print(var1)
print(var2)
print(var3)


